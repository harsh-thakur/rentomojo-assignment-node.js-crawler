# Medium _ Crawler

A Node.js web crawler.
Recursively​ crawling popular blogging website ​ https://medium.com​ using Node.js and harvesting all possible
hyperlinks that belong to ​ medium.com​ and store them in a MongoDB database.

# Instructions to install on local machine 

    1. Run Command - git clone https://gitlab.com/harsh-thakur/rentomojo-assignment-node.js-crawler.git
    2. Run npm install
    3. npm run start

# Docker Image 
 Building your image 
    1. docker build -t <your username>/node-web-app .
 Run your docker image
    2. docker run -p 49160:4000 -d <your username>/node-web-app  
   Test
   To test your app, get the port of your app that Docker mapped: 
    
   $ docker ps

  # Example
   ID            IMAGE                                COMMAND    ...   PORTS
   ecce33b30ebf  <your username>/node-web-app:latest  npm start  ...   49160->4000

   Now you can call your app using curl (install if needed via: sudo apt-get install curl):
   
   $ curl -i localhost:49160

   Already created Docker image-     
   Repository - rentomojo-assignment-node.js-crawler
   Image Id - a18eea3297bd

# To Retrieve the already parsed urls-
   Hit the URL in your browser - localhost:`${portOnWhichYourLocalServerIsRunning}`/api/v1/retrieveParsedUrls

# To Start the Process of Scraping afresh-
   Hit the URL in your browser - localhost:`${portOnWhichYourLocalServerIsRunning}`/api/v1/startCrawling

   And after 1-2 minutes again hit the URl - localhost:`${portOnWhichYourLocalServerIsRunning}`/api/v1/retrieveParsedUrls
    to see the parsed urls

# NPM packages Used-
   "body-parser": "^1.19.0",
   "cheerio": "^1.0.0-rc.3",
   "cors": "^2.8.5",
   "dotenv": "^7.0.0",
   "express": "^4.16.4",
   "mongoose": "^5.5.4",
   "request": "^2.88.0"