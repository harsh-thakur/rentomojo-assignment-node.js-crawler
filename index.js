// third party libs
require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const mongoose = require('mongoose');

const ApiRoutes = require('./src/routes/api');


var app = express();
var server = http.createServer(app);

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET, POST');
    next();
});

mongoose.Promise = global.Promise;
const option = {
    socketTimeoutMS: 0,
    connectTimeoutMS: 0,
    useNewUrlParser: true
};
// connect to db
mongoose.connect(process.env.mongodbConnectionString,option,(err)=>{
    if(err){
        console.error(err.stack)
        process.exit(1)
    }else{
        console.log('Connected!!!');
    }
})


app.use('/api/v1', ApiRoutes);

app.set('port', (process.env.PORT || 80));
server.listen(app.get('port'), function () {
    console.log('Server started on port ' + app.get('port'));
});

