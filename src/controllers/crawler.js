var request = require('request'); // To make http requests.
var cheerio = require('cheerio');  // To parse HTML
const SaveAndUpdateDB = require('./saveAndUpdateDB');
const ParsedUrls = require('../Models/parsedUrlModel') // Model
const util = require('util');  // To promisify


// Function to Crawl the website by URLs by calling it resursively inside run function.
const scrape = (url) => {
    return new Promise((resolve, reject) => {
        let taskUrls = [];
        request(url, (err, res, body) => {
            if (err) resolve([]);  //to handle ETIMEDOUT error
            if (body) {
                $ = cheerio.load(body);
                links = $('a');
                $(links).each(function (i, link) {
                    taskUrls.push($(link).attr('href'));
                }, resolve(taskUrls));
            } else {
                resolve(taskUrls)
            }
        })
    })
}

// Initial URL queue to crawl
var tasks = [
    'https://medium.com/'
];

// Promise Queue class to schedule urls to be crawled.
class PromiseQueue {

    // Initializing First Time.
    constructor(urls = [], concurrentCount = 1) {
        this.concurrent = concurrentCount - 1; // For Cuncurrency 
        this.todo = urls;                    // Queue of Urls  
        this.running = [];                   // Currently Running 
        this.complete = [];                  // Completed Urls       
        this.completedUrls = {};             // Completed URLs object to mark the visited and completed Urls   
    }

    // Getter function to check whether to run another task(url) to get crawled recursively.
    get runAnother() {
        return (this.running.length < this.concurrent && this.todo.length);
    }

    // Checking if the Url to be processed is valid or not.
    checkIfUrlIsValid(url) {
        const pattern = new RegExp('^(https?:\\/\\/)?' +
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,})' +
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' +
            '(\\?[;&a-z\\d%_.~+=-]*)?' +
            '(\\#[-a-z\\d_]*)?$', 'i');
        return !!pattern.test(url);
    }

    /* Function to check if a particular URL belong to https://medium.com, otherwise it might get to 
    antoher websites and the process could never stop*/
    checkIfBelongTOMedium(url) {
        let pattern = 'medium.com';
        pattern = new RegExp(pattern);
        return pattern.test(url);

    }

    /* Remove Already processed Urls */
    removeDuplicateUrls(taskUrls, completedUrlsObject, callback) {
        return new Promise(async (resolve, reject) => {
            let updatedUrls = []
            updatedUrls = taskUrls.filter(url => {
                let u = url;
                if (completedUrlsObject[u] != true && this.checkIfUrlIsValid(url)) { return true; }
                return false;
            })
            resolve(updatedUrls);
        })
    }

    /* Recursive Function*/
    recursiveSolution() {
        while (this.runAnother) {
            var url = this.todo.shift();
            //Check if already processed, url is valid and belong to medium.com
            if (!this.completedUrls[url] && this.checkIfUrlIsValid(url) && this.checkIfBelongTOMedium(url)) {
                this.completedUrls[url] = true;

                //promise to crawl particular url.
                scrape(url).then((taskUrls) => {

                    // If scraped url array is not NULL then remove all the duplicates and add to the todo Queue.
                    if (taskUrls.length > 0) {
                        this.removeDuplicateUrls(taskUrls, this.completedUrls).then(uniqueUrls => {
                            this.todo = [...this.todo, ...uniqueUrls]
                            this.complete.push(this.running.shift());

                            // If schedule queue is empty or completed task has reached to 50 Save them to DB.
                            if (this.todo.length == 0 || this.complete.length == 50) { 
                                scrapingDone(this.complete)
                                this.complete.length = 0;
                            }
                            this.recursiveSolution(); //call recursive function for another url to crawl.
                        }).catch(err => {
                            // If any error occurs save completed task to the DB.
                            scrapingDone(this.complete);
                        })

                    } else {
                        this.todo = [...this.todo, ...taskUrls];
                        this.complete.push(this.running.shift()); // Push to the complete array.

                        if (this.todo.length == 0 || this.complete.length == 50) {
                            scrapingDone(this.complete)
                            this.complete.length = 0;
                        }
                        this.recursiveSolution(); // Calling Resursive Function.
                    }

                }).catch(err => {
                    this.recursiveSolution();

                    scrapingDone(this.complete)
                })
                this.running.push(url); // Pushing current url to The Running Queue.
            }
        }
    }
}

var startQueue = new PromiseQueue(tasks, 5); // Initialize Promise Queue with tasks any concurrent operations to be maintained.

// function to Save the parsed Urls to Database.
function scrapingDone(urlsArray) {
    SaveAndUpdateDB.saveAndUpdate_DB(urlsArray);
    
}


// Driver Function To be called from routes file.
function callRun(req, res) {
    startQueue.recursiveSolution();
    res.json({
        msg: 'Scraping Procedure have been initiated, Please replace "startCrawling" with "retrieveParsedUrls" int the above Url to see the solution!'
    })

    dropCollectionAsync();

}

// Drop collection and save new data.
const dropCollection = () => {
    ParsedUrls.find({}).then(ifExists => {
        if (ifExists.length > 0) {
            ParsedUrls.collection.drop();
        }
    });

}
const dropCollectionAsync = util.promisify(dropCollection);


//Error handling

process.on('uncaughtException', function (err) {
    console.log(err)
})

module.exports = { startQueue, callRun };
