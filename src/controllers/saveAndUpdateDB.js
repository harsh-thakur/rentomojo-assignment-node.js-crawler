const ParsedUrls = require('../Models/parsedUrlModel');
const util = require('util');


// Function to modify Url in the form to be saved in DB.
desieredUrlForm =  (urlsArray,callback)=>{
   let urls_arrayObject = [];
      urlsArray.map(url=>{
        let splitedUrl = url.split('?');//https://medium.com?source=test => ["https://medium.com","source=test"]
        let obj ={}
        obj.url = splitedUrl[0]; //"https://medium.com"
        splitedUrl.shift();
        obj.parameters = splitedUrl.map(urlParams=>{  // looping through all the params and spliting on "=" extracting them e.g ["source","test"]
            return urlParams.split('=')[0];
        })
        // console.log(obj)
        urls_arrayObject.push(obj);  // trnasformed array object of Urls
    })
    callback(null,urls_arrayObject); // After all the Urls are processed callback function
}

const waitForUrlsToGetParsed = util.promisify(desieredUrlForm);


// Database Operations 

saveAndUpdate_DB = async (urlsArray)=>{

    waitForUrlsToGetParsed(urlsArray).then(async modifiedArrayObject=>{ 
        for (let i = 0; i < modifiedArrayObject.length; i++) {
            let element = modifiedArrayObject[i];
            let isFoundAndUpdated=  await ParsedUrls.findOne({ url: element.url })
                // Check if a document with current Url is already created or not.
                //If not created then create and save it Database.
                if (!isFoundAndUpdated) { 
                    let create = new ParsedUrls();
                    create.url = element.url;
                    create.referenceCount = 1;
                    create.parameters = element.parameters;
                    await create.save();
                } else {
                    // If it has already been created then update the existing document and save to Database.
                    isFoundAndUpdated.referenceCount++;
                    for (let i = 0; i < element.parameters.length; i++) {
                        //Uniquely push parameters.
                        if (isFoundAndUpdated.parameters.indexOf(element.parameters[i]) == -1) isFoundAndUpdated.parameters.push(element.parameters[i]);
                    }
                    new ParsedUrls(isFoundAndUpdated);
                   await isFoundAndUpdated.save();
                } 

        }
    })
}


module.exports = {saveAndUpdate_DB};