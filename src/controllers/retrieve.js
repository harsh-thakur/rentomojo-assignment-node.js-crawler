const ParsedUrls = require('../Models/parsedUrlModel');


// Get function to retrieve parsed urls from Database.
getParsedUrls = async (req,res)=>{
    try {
        let parsedUrls = await ParsedUrls.find({});
        if(parsedUrls && parsedUrls.length>0){
            res.send({
                success:true,
                NoOfUniqueUrls:parsedUrls.length || 0,
                parsedUrls:parsedUrls

            })
        }else{
            res.send({
                success:true,
                parsedUrls:'No urls have been parsed Yet!!'
            })
        }
        
    } catch (error) {
        res.send({
            success:false,
            msg:error
        })
    }

}

module.exports = { getParsedUrls};