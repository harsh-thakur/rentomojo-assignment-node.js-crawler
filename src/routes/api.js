const express = require('express');
const router = express.Router();
const CrawlerController = require('../controllers/crawler');
const Retrieve = require('../controllers/retrieve');


router.get('/', function (req, res) {
    res.json({
        'API': '1.0'
    });
});

router.get('/startCrawling',CrawlerController.callRun);
router.get('/retrieveParsedUrls', Retrieve.getParsedUrls);

module.exports = router;